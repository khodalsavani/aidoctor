import { Injectable } from '@angular/core';

@Injectable({
    providedIn: 'root'
})
export class GlobalService {

    constructor() { }

    public static serverURL = 'http://3.20.180.92/ai-doctor/'; // demo live
    public static imageURL = 'http://3.20.180.92/ai-doctor/'; // demo live
}
