import { Injectable } from '@angular/core';

import { GlobalService } from '../global/global.service';
import { HttpClient, HttpHeaders } from '@angular/common/http';

@Injectable({
    providedIn: 'root'
})
export class PatientService {

    public url: string = GlobalService.serverURL;

    constructor(
        private http: HttpClient
    ) { }

    getProblemsList = (): any => {
        return this.http.get(this.url + 'rest-api-service/problem-management/listing').toPromise();
    }

    getSubProblemsList = (problemId): any => {
        return this.http.get(this.url + 'rest-api-service/problem-management/sub-problem/listing?problem_id=' + problemId).toPromise();
    }

    getSubProblemsDetails = (subProblemId): any => {
        return this.http.get(this.url + 'rest-api-service/problem-management/sub-problem/detail?sub_problem_id=' + subProblemId).toPromise();
    }

    getMedicationsList = (): any => {
        return this.http.get(this.url + 'rest-api-service/medication-management/listing').toPromise();
    }

    getSubMedicationsList = (problemId): any => {
        return this.http.get(this.url + 'rest-api-service/medication-management/sub-medication/listing?medication_id=' + problemId).toPromise();
    }

    getSubMedicationsDetails = (subMediId): any => {
        return this.http.get(this.url + 'rest-api-service/medication-management/sub-medication/detail?sub_medication_id=' + subMediId).toPromise();
    }
}
