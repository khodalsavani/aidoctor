import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { PatientProblemDetailsPage } from './patient-problem-details.page';

const routes: Routes = [
  {
    path: '',
    component: PatientProblemDetailsPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class PatientProblemDetailsPageRoutingModule {}
