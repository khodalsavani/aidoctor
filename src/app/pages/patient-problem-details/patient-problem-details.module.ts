import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { PatientProblemDetailsPageRoutingModule } from './patient-problem-details-routing.module';

import { PatientProblemDetailsPage } from './patient-problem-details.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    PatientProblemDetailsPageRoutingModule
  ],
  declarations: [PatientProblemDetailsPage]
})
export class PatientProblemDetailsPageModule {}
