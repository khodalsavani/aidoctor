import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { LoadingController } from '@ionic/angular';
import { PatientService } from 'src/app/service/patient/patient.service';

@Component({
    selector: 'app-patient-problem-details',
    templateUrl: './patient-problem-details.page.html',
    styleUrls: ['./patient-problem-details.page.scss'],
})
export class PatientProblemDetailsPage implements OnInit {

    public getParams: any = {};
    public loading: any;
    public subProblemDetails: any = {};

    constructor(
        private loadingCtrl: LoadingController,
        private route: ActivatedRoute,
        private patientService: PatientService
    ) { }

    ngOnInit() {
        this.route.params.subscribe(params => {
            console.log('detail params', params);
            this.getParams = JSON.parse(params.data);
            this.getSubProblemsDetails(this.getParams.sub_problem_id);
        });
    }

    async presentLoading() {
        this.loading = await this.loadingCtrl.create({
            message: 'Please wait...'
        });

        return await this.loading.present();
    }

    getSubProblemsDetails = (id): void => {
        // this.presentLoading();
        this.patientService.getSubProblemsDetails(id).then((success) => {
            console.log('getSubProblemsDetails success', success);
            // this.loading.dismiss();
            if (success.status == 200) {
                this.subProblemDetails = success.data;
            }
        }, (error) => {
            console.log('error', error);
            // this.loading.dismiss();
        });
    }

}
