import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { PatientTabPage } from './patient-tab.page';

const routes: Routes = [
    {
        path: '../pages/patient-tab',
        component: PatientTabPage,
        children: [
            {
                path: 'patient-search',
                loadChildren: () => import('./../patient-search/patient-search.module').then(m => m.PatientSearchPageModule)
            }
        ]
    },
    {
        path: '',
        redirectTo: '../pages/patient-tab/patient-search',
        pathMatch: 'full'
    }
];


// const routes: Routes = [
//     {
//         path: './pages/patient-tab',
//         component: PatientTabPage,
//         children: [
//             {
//                 path: 'patient-search',
//                 children: [
//                     {
//                         path: '',
//                         loadChildren: '../patient-search/patient-search.module#PatientSearchPageModule'
//                     }
//                 ]
//             },
//             {
//                 path: '',
//                 redirectTo: './pages/patient-tab/patient-search',
//                 pathMatch: 'full'
//             }
//         ]
//     },
//     {
//         path: '',
//         redirectTo: './pages/patient-tab/patient-search',
//         pathMatch: 'full'
//     }
// ];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule],
})
export class PatientTabPageRoutingModule { }
