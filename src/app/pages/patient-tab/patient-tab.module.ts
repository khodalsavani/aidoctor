import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { PatientTabPageRoutingModule } from './patient-tab-routing.module';

import { PatientTabPage } from './patient-tab.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    PatientTabPageRoutingModule
  ],
  declarations: [PatientTabPage]
})
export class PatientTabPageModule {}
