import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { PatientTabPage } from './patient-tab.page';

describe('PatientTabPage', () => {
  let component: PatientTabPage;
  let fixture: ComponentFixture<PatientTabPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PatientTabPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(PatientTabPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
