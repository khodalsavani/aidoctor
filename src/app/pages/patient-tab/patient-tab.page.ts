import { Component, OnInit, ViewChild } from '@angular/core';
import { IonTabs } from '@ionic/angular';

@Component({
    selector: 'app-patient-tab',
    templateUrl: './patient-tab.page.html',
    styleUrls: ['./patient-tab.page.scss'],
})
export class PatientTabPage implements OnInit {

    @ViewChild('myTabs') tabRef: IonTabs;
    public tabsData: any = [];

    constructor() { }

    ngOnInit() {
        this.tabsData = [
            {
                id: 1,
                name: 'Search',
                tab: 'patient-search',
                img: './assets/images/search.svg',
            }
        ];
    }

    tabChange(myTab) {
        console.log('myTab', myTab);
        for (let i = 0; i < this.tabsData.length; i++) {
            if (this.tabsData[i].name == myTab.name) {
                this.tabsData[i].isSelected = true;
            } else {
                this.tabsData[i].isSelected = false;
            }
        }
    }

}
