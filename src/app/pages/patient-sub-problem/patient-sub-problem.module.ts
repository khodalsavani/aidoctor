import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { PatientSubProblemPageRoutingModule } from './patient-sub-problem-routing.module';

import { PatientSubProblemPage } from './patient-sub-problem.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    PatientSubProblemPageRoutingModule
  ],
  declarations: [PatientSubProblemPage]
})
export class PatientSubProblemPageModule {}
