import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { PatientSubProblemPage } from './patient-sub-problem.page';

describe('PatientSubProblemPage', () => {
  let component: PatientSubProblemPage;
  let fixture: ComponentFixture<PatientSubProblemPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PatientSubProblemPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(PatientSubProblemPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
