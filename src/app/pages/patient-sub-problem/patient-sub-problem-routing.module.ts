import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { PatientSubProblemPage } from './patient-sub-problem.page';

const routes: Routes = [
  {
    path: '',
    component: PatientSubProblemPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class PatientSubProblemPageRoutingModule {}
