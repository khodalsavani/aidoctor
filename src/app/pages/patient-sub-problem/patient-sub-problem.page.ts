import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { LoadingController, NavController } from '@ionic/angular';
import { PatientService } from 'src/app/service/patient/patient.service';
import { GlobalService } from 'src/app/service/global/global.service';

@Component({
    selector: 'app-patient-sub-problem',
    templateUrl: './patient-sub-problem.page.html',
    styleUrls: ['./patient-sub-problem.page.scss'],
})
export class PatientSubProblemPage implements OnInit {

    public loading: any;
    public getSubProblemsData: any = [];
    public getParams: any = {};
    public imageURL: string = GlobalService.imageURL;

    constructor(
        private loadingCtrl: LoadingController,
        private route: ActivatedRoute,
        private patientService: PatientService,
        private navCtrl: NavController
    ) { }

    ngOnInit() {
        this.route.params.subscribe(params => {
            console.log('detail params', params);
            this.getParams = JSON.parse(params.data);
            this.getSubProblem(this.getParams.problem_id)
        });
    }

    async presentLoading() {
        this.loading = await this.loadingCtrl.create({
            message: 'Please wait...'
        });

        return await this.loading.present();
    }

    getSubProblem = (id): void => {
        // this.presentLoading();
        this.patientService.getSubProblemsList(id).then((success) => {
            console.log('getSubProblemsList success', success);
            // this.loading.dismiss();
            if (success.status == 200) {
                this.getSubProblemsData = success.data;
            }
        }, (error) => {
            console.log('error', error);
            // this.loading.dismiss();
        });
    }

    subProblemView = (data): void => {
        this.navCtrl.navigateForward(['patient-problem-details', {
            data: JSON.stringify(data)
        }]);
    }

}
