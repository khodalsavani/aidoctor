import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { PatientMedicationDetailsPageRoutingModule } from './patient-medication-details-routing.module';

import { PatientMedicationDetailsPage } from './patient-medication-details.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    PatientMedicationDetailsPageRoutingModule
  ],
  declarations: [PatientMedicationDetailsPage]
})
export class PatientMedicationDetailsPageModule {}
