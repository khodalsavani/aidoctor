import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { PatientService } from 'src/app/service/patient/patient.service';
import { LoadingController } from '@ionic/angular';

@Component({
    selector: 'app-patient-medication-details',
    templateUrl: './patient-medication-details.page.html',
    styleUrls: ['./patient-medication-details.page.scss'],
})
export class PatientMedicationDetailsPage implements OnInit {

    public getParams: any = {};
    public loading: any;
    public subMedicationDetails: any = {};

    constructor(
        private loadingCtrl: LoadingController,
        private route: ActivatedRoute,
        private patientService: PatientService
    ) { }

    ngOnInit() {
        this.route.params.subscribe(params => {
            console.log('detail params', params);
            this.getParams = JSON.parse(params.data);
            this.getSubProblemsDetails(this.getParams.sub_medication_id);
        });
    }

    async presentLoading() {
        this.loading = await this.loadingCtrl.create({
            message: 'Please wait...'
        });

        return await this.loading.present();
    }

    getSubProblemsDetails = (id): void => {
        // this.presentLoading();
        this.patientService.getSubMedicationsDetails(id).then((success) => {
            console.log('getSubProblemsDetails success', success);
            // this.loading.dismiss();
            if (success.status == 200) {
                this.subMedicationDetails = success.data;
            }
        }, (error) => {
            console.log('error', error);
            // this.loading.dismiss();
        });
    }

}
