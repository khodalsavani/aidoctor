import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { PatientSearchPage } from './patient-search.page';

describe('PatientSearchPage', () => {
  let component: PatientSearchPage;
  let fixture: ComponentFixture<PatientSearchPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PatientSearchPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(PatientSearchPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
