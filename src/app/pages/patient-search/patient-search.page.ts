import { Component, OnInit } from '@angular/core';
import { LoadingController, NavController } from '@ionic/angular';
import { PatientService } from 'src/app/service/patient/patient.service';
import { GlobalService } from 'src/app/service/global/global.service';

@Component({
    selector: 'app-patient-search',
    templateUrl: './patient-search.page.html',
    styleUrls: ['./patient-search.page.scss'],
})
export class PatientSearchPage implements OnInit {

    public showClose: boolean = false;
    public searchText: any;
    public loading: any;
    public selectSegment: string = 'problem';
    public problemsData: any = [];
    public tempProblemsData: any = [];
    public medicationData: any = [];
    public tempMedicationData: any = [];
    public imageURL: string = GlobalService.imageURL;
    public segmentData: any = [];

    constructor(
        private loadingCtrl: LoadingController,
        private patientService: PatientService,
        private navCtrl: NavController
    ) { }

    ngOnInit() {
        this.segmentData = [
            {
                id: 1,
                name: 'Problem',
                value: 'problem',
                isSelected: true
            },
            {
                id: 2,
                name: 'Medication',
                value: 'medication',
                isSelected: false
            }
        ]
        this.problemClick();
    }

    async presentLoading() {
        this.loading = await this.loadingCtrl.create({
            message: 'Please wait...'
        });

        return await this.loading.present();
    }

    changeSegment = (data): void => {
        this.onClearClick();
        for (let segment of this.segmentData) {
            if (segment.id == data.id) {
                segment.isSelected = true;
                this.selectSegment = segment.value;

                if (this.selectSegment == 'problem') {
                    this.problemClick();
                } else if (this.selectSegment == 'medication') {
                    this.medicationClick();
                }
            } else {
                segment.isSelected = false;
            }
        }
    }

    problemClick = (): void => {
        // this.presentLoading();
        this.patientService.getProblemsList().then((success) => {
            console.log('getProblemsList success', success);
            // this.loading.dismiss();
            if (success.status == 200) {
                this.problemsData = success.data;
                this.tempProblemsData = this.problemsData;
            }
        }, (error) => {
            console.log('error', error);
            // this.loading.dismiss();
        });
    }

    medicationClick = (): void => {
        // this.presentLoading();
        this.patientService.getMedicationsList().then((success) => {
            console.log('getMedicationsList success', success);
            // this.loading.dismiss();
            if (success.status == 200) {
                this.medicationData = success.data;
                this.tempMedicationData = this.medicationData;
            }
        }, (error) => {
            console.log('error', error);
            // this.loading.dismiss();
        });
    }

    onKey(event: any) {
        console.log('searchText', this.searchText);
        this.showClose = true;
        if (this.selectSegment == 'problem') {
            if (this.searchText && this.searchText.trim() != '') {
                this.problemsData = this.tempProblemsData.filter((item) => {
                    return (item.title.toLowerCase().indexOf(this.searchText.toLowerCase()) > -1);
                });
            }
        } else if (this.selectSegment == 'medication') {
            if (this.searchText && this.searchText.trim() != '') {
                this.medicationData = this.tempMedicationData.filter((item) => {
                    return (item.title.toLowerCase().indexOf(this.searchText.toLowerCase()) > -1);
                });
            }
        }
    }

    onClearClick() {
        this.searchText = "";
        this.showClose = false;

        if (this.selectSegment == 'problem') {
            this.problemsData = this.tempProblemsData;
        } else if (this.selectSegment == 'medication') {
            this.medicationData = this.tempMedicationData;
        }
    }

    problemView = (data): void => {
        this.navCtrl.navigateForward(['patient-sub-problem', {
            data: JSON.stringify(data)
        }]);
    }

    medicationView = (data): void => {
        this.navCtrl.navigateForward(['patient-sub-medications', {
            data: JSON.stringify(data)
        }]);
    }

}
