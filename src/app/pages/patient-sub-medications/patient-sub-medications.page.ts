import { Component, OnInit } from '@angular/core';
import { GlobalService } from 'src/app/service/global/global.service';
import { LoadingController, NavController } from '@ionic/angular';
import { ActivatedRoute } from '@angular/router';
import { PatientService } from 'src/app/service/patient/patient.service';

@Component({
    selector: 'app-patient-sub-medications',
    templateUrl: './patient-sub-medications.page.html',
    styleUrls: ['./patient-sub-medications.page.scss'],
})
export class PatientSubMedicationsPage implements OnInit {

    public loading: any;
    public getSubMedicationsData: any = [];
    public getParams: any = {};
    public imageURL: string = GlobalService.imageURL;

    constructor(
        private loadingCtrl: LoadingController,
        private route: ActivatedRoute,
        private patientService: PatientService,
        private navCtrl: NavController
    ) { }

    ngOnInit() {
        this.route.params.subscribe(params => {
            console.log('detail params', params);
            this.getParams = JSON.parse(params.data);
            this.getSubmMedication(this.getParams.medication_id)
        });
    }

    async presentLoading() {
        this.loading = await this.loadingCtrl.create({
            message: 'Please wait...'
        });

        return await this.loading.present();
    }

    getSubmMedication = (id): void => {
        // this.presentLoading();
        this.patientService.getSubMedicationsList(id).then((success) => {
            console.log('getSubProblemsList success', success);
            // this.loading.dismiss();
            if (success.status == 200) {
                this.getSubMedicationsData = success.data;
            }
        }, (error) => {
            console.log('error', error);
            // this.loading.dismiss();
        });
    }

    subMedicationView = (data): void => {
        this.navCtrl.navigateForward(['patient-medication-details', {
            data: JSON.stringify(data)
        }]);
    }
}
