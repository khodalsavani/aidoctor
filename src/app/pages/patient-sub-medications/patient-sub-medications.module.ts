import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { PatientSubMedicationsPageRoutingModule } from './patient-sub-medications-routing.module';

import { PatientSubMedicationsPage } from './patient-sub-medications.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    PatientSubMedicationsPageRoutingModule
  ],
  declarations: [PatientSubMedicationsPage]
})
export class PatientSubMedicationsPageModule {}
