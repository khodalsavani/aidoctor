import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { PatientSubMedicationsPage } from './patient-sub-medications.page';

describe('PatientSubMedicationsPage', () => {
  let component: PatientSubMedicationsPage;
  let fixture: ComponentFixture<PatientSubMedicationsPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PatientSubMedicationsPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(PatientSubMedicationsPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
