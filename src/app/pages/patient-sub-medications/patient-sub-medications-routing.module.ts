import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { PatientSubMedicationsPage } from './patient-sub-medications.page';

const routes: Routes = [
  {
    path: '',
    component: PatientSubMedicationsPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class PatientSubMedicationsPageRoutingModule {}
