import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';

const routes: Routes = [
    {
        path: '', // patient-tab
        loadChildren: () => import('./pages/patient-tab/patient-tab.module').then(m => m.PatientTabPageModule)
    },
  {
    path: 'patient-sub-problem',
    loadChildren: () => import('./pages/patient-sub-problem/patient-sub-problem.module').then( m => m.PatientSubProblemPageModule)
  },
  {
    path: 'patient-problem-details',
    loadChildren: () => import('./pages/patient-problem-details/patient-problem-details.module').then( m => m.PatientProblemDetailsPageModule)
  },
  {
    path: 'patient-sub-medications',
    loadChildren: () => import('./pages/patient-sub-medications/patient-sub-medications.module').then( m => m.PatientSubMedicationsPageModule)
  },
  {
    path: 'patient-medication-details',
    loadChildren: () => import('./pages/patient-medication-details/patient-medication-details.module').then( m => m.PatientMedicationDetailsPageModule)
  }
];
@NgModule({
    imports: [
        RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
    ],
    exports: [RouterModule]
})
export class AppRoutingModule { }
